//Copyright (c) 2018 Bhushan Kalse.

#include <ESP8266WiFi.h>

const int BTN_GREEN   = 5;  //PIN D1
const int BTN_RED     = 4;  //PIN D2
const int BTN_WHITE   = 0;  //PIN D3
const int BTN_BLUE    = 2;  //PIN D4
const int BTN_YELLOW  = 14; //PIN D5

#define CLR_YELLOW  0
#define CLR_GREEN   1
#define CLR_RED     2
#define CLR_BLUE    3
#define CLR_WHITE   4

#define ON    true
#define OFF   false
#define DEBUG ON

int lastColorChanged = -1;
int switchPressCount = 0;

const char *ssid      = "WIFI_SSID";  	// Your WiFi SSID
const char *password  = "WIFI_PWD";		// WiFi PWD
const char *api_key   = "LIFX_API_KEY";	// Lifx API Key
const char *host      = "api.lifx.com";
const int port        = 443;
const int MaxMsgLength = 1500;

void TurnLightON();
void TurnLightOFF();
bool IsBtnPressed(int button);
void DebugPrint(String str);
void DebugPrintln(String str);
void SendColorToLifx(int color);
void ChangeLightState(String state);
void SendRequestToLifx(String reqType, String url, String data);

void setup() 
{
  if(DEBUG == ON)
  {
    Serial.begin(115200);
    delay(100);
  }
  
  pinMode (BTN_YELLOW, INPUT_PULLUP);
  pinMode (BTN_GREEN,  INPUT_PULLUP);
  pinMode (BTN_RED,    INPUT_PULLUP);
  pinMode (BTN_BLUE,   INPUT_PULLUP);
  pinMode (BTN_WHITE,  INPUT_PULLUP);
  
  // We start by connecting to a WiFi network
  DebugPrintln(" ");
  DebugPrintln(" ");
  DebugPrint("Connecting to ");
  DebugPrintln(ssid);
  
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) 
  {
    delay(500);
    DebugPrint("-");
  }

  DebugPrintln("");
  DebugPrintln("WiFi connected");
  DebugPrintln("IP address: ");
  DebugPrintln(WiFi.localIP().toString()); 

  TurnLightON();
}

void loop() 
{ 
  if(IsBtnPressed(BTN_YELLOW) == true)
  {    
     SendColorToLifx(CLR_YELLOW);
  }
  
  if(IsBtnPressed(BTN_GREEN) == true)
  {
     SendColorToLifx(CLR_GREEN);
  }

  if(IsBtnPressed(BTN_RED) == true)
  {     
     SendColorToLifx(CLR_RED);
  }

  if(IsBtnPressed(BTN_BLUE) == true)
  {    
     SendColorToLifx(CLR_BLUE);
  }

  if(IsBtnPressed(BTN_WHITE) == true)
  {     
     SendColorToLifx(CLR_WHITE);
  }

  delay(10); // Wait for a moment 
}

void TurnLightON()
{
  ChangeLightState("on");
}

void TurnLightOFF()
{
  ChangeLightState("off");
}

void ChangeLightState(String state) // state: on/off
{       
    String url = "https://api.lifx.com/v1/lights/all/state";
    String data = "{\r\n";
           data += "\"power\": \"" + state + "\",\r\n";  
           data += "\"fast\": false\r\n";  
           data += "}\r\n";
      
    SendRequestToLifx("PUT", url, data);
}

void ChangeLightColor(String color) 
{       
    String url = "https://api.lifx.com/v1/lights/all/state";
    String data = "{\r\n";
           data += "\"color\": \"" + color + "\",\r\n";  
           data += "\"fast\": false\r\n";  
           data += "}\r\n";
      
    SendRequestToLifx("PUT", url, data);
}

void SendColorToLifx(int color)
{

  if(lastColorChanged == color)
  {
    switchPressCount++;
    DebugPrintln(String(switchPressCount));
    
    // Check if we are turnning the light off.
    if(switchPressCount >= 8 && IsBtnPressed(BTN_WHITE) == true) // 8: magic number.
    {
      TurnLightOFF();
    }
    return;
  }
    
  switch(color)
  {
    case CLR_YELLOW:    
      ChangeLightColor("yellow");
      DebugPrintln("BTN_YELLOW");
    break;
    
    case CLR_GREEN:
      DebugPrintln("CLR_GREEN");
      ChangeLightColor("green");      
    break;
    
    case CLR_RED:
      DebugPrintln("CLR_RED");
      ChangeLightColor("red");      
    break;
    
    case CLR_BLUE:
      DebugPrintln("CLR_BLUE");
      ChangeLightColor("3349FF");      
    break;
    
    case CLR_WHITE:
      DebugPrintln("CLR_WHITE");
      ChangeLightColor("white");      
    break;    
  }

  lastColorChanged = color;
  switchPressCount = 0;
}

void SendRequestToLifx(String reqType, String url, String data)
{
    WiFiClientSecure client;
    DebugPrintln("Connecting to " + String(host) + " on port " + String(port));
    int ret = client.connect(host, port);
    
    if (ret == false) 
    {
        DebugPrintln("Connection failed: " + String(ret));
        return;
    } 
    
    String header = "";
    String body = "";
    bool headerRecved = false;
    bool emptyLine = true;
    long now;
    int charCount = 0;
    
    client.print(reqType + " " + url + " HTTP/1.1\r\n" +
                "Host: " + host + "\r\n" + 
                "Authorization: Bearer "+ api_key +"\r\n" +
                "Connection: close\r\n" +
                "Content-Type: application/json\r\n" +
                "Content-Length: " + String(data.length()) + "\r\n\r\n" +
                data);
    
    //delay(500); 
    now = millis();
    delay(1);

    DebugPrintln("Sent '" + reqType + "' to " + url);
    DebugPrintln("Data: " +  data);
   //*
    while (millis() - now < 3000) 
    {
        while (client.available()) 
        {
            char clientData = client.read();
    
            if(headerRecved == false)
            {
                if (emptyLine == true && clientData == '\n') 
                {
                    headerRecved = true;
                    DebugPrintln("Header Received.");
                }
                else
                {
                    header += clientData;
                }
            } 
            else 
            {
                if (charCount < MaxMsgLength)  
                {
                    body += clientData;
                    charCount++;
                }
                else
                {
                    DebugPrintln("Message length over the limit: " + String(charCount));
                }
            }
    
            if (clientData == '\n') 
            {
                emptyLine = true;
            }
            else if (clientData != '\r') 
            {
                emptyLine = false;
            }    
        }
    }
    DebugPrintln(">> " + body);
//*/
}

void DebugPrint(String str = "")
{
  if(DEBUG == ON) 
    Serial.print(str);
}

void DebugPrintln(String str = "")
{
  if(DEBUG == ON) 
    Serial.println(str);
}

bool IsBtnPressed(int button)
{
  return digitalRead(button) == LOW;
}

