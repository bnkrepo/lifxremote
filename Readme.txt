ESP8266 code to remotely control LIFX Bulb. 
5 hardware switches are connected to esp8266 on D1, D2, D3, D4, D5 pins.
Initially tried using the library at https://github.com/joeybab3/arduino-lifx-api but in the end didn't use it. 
This code is not dependent on any third party library. 

Setup Arduino IDE for ESP8266 environment. That is all needed for this code to work. There is a blog entry for this project at: 
https://bnkalse.wordpress.com/2018/11/11/esp8266-lifx-api-toy-for-a-toddler/
